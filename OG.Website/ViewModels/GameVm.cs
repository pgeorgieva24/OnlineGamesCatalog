﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OG.Website.ViewModels
{
    public class GameVm
    {
        public int Id { get; set; }
        [StringLength(200, ErrorMessage = "Name should not be longer than 200 symbols")]
        [Required]
        public string Name { get; set; }
        [StringLength(4)]
        [Required]
        public string ReleaseYear { get; set; }
        public int GenreId { get; set; }
        public int RatingId { get; set; }
        public GenreVm Genre { get; set; }
        public RatingVm Rating { get; set; }
    }
}