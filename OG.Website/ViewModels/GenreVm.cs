﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OG.Website.ViewModels
{
    public class GenreVm
    {
        public int Id { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "Name should not be longer than 100 symbols")]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}