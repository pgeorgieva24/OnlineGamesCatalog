﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OG.Website.ViewModels
{
    public class RatingVm
    {
        public int Id { get; set; }
        [StringLength(100, ErrorMessage = "Rating Value should not be longer than 100 symbols")]
        [Required]
        public string RatingValue { get; set; }
        [StringLength(400, ErrorMessage = "Description should not be longer than 400 symbols")]
        [Required]
        public string Description { get; set; }
    }
}