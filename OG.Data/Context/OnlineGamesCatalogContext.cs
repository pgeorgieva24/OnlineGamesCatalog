﻿using OG.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OG.Data.Context
{
    public class OnlineGamesCatalogContext : DbContext
    {
        public OnlineGamesCatalogContext():base(@"Server=.\SQLEXPRESS;Database=OnlineGamesCatalog;Trusted_Connection=True;")
        {

        }

        public DbSet<Game> Games { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Rating> Ratings { get; set; }

    }
}
