﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OG.Data.Entities
{
    public class Rating : BaseEntity
    {
        [MinLength(1), MaxLength(100)]
        public string RatingValue { get; set; }
        [MinLength(1), MaxLength(400)]
        public string Description { get; set; }
    }
}
