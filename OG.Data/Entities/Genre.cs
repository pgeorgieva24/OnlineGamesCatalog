﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OG.Data.Entities
{
    public class Genre : BaseEntity
    {
        [MinLength(1), MaxLength(100)]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
