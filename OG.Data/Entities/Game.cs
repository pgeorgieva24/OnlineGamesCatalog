﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OG.Data.Entities
{
    public class Game : BaseEntity
    {
        [MinLength(1), MaxLength(200)]
        public string Name { get; set; }
        [StringLength(4)]
        public string ReleaseYear { get; set; }
        public int GenreId { get; set; }
        public virtual Genre Genre { get; set; }
        public int RatingId { get; set; }
        public virtual Rating Rating { get; set; }

    }
}
