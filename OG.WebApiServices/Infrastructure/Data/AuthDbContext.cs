﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OG.WebApiServices.Infrastructure.Data
{
    public class AuthDbContext : IdentityDbContext<IdentityUser>
    {
        public AuthDbContext() : base(@"Server=.\SQLEXPRESS;Database=OnlineGamesCatalog;Trusted_Connection=True;")
        {

        }
    }
}