﻿using OG.ApplicationServices.DTOs;
using OG.ApplicationServices.Implementations;
using OG.WebApiServices.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OG.WebApiServices.Controllers
{
    [Authorize]
    public class RatingsController : ApiController
    {
        private readonly RatingManagementService service = null;

        public RatingsController()
        {
            service = new RatingManagementService();
        }

        [HttpGet]
        public IHttpActionResult GetAll()
        {
            return Json(service.Get());
        }

        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            return Json(service.GetById(id));
        }

        [HttpPost]
        public IHttpActionResult Save(RatingDto ratingDto)
        {
            if (!ratingDto.Validate())
                return Json(new ResponseMessage { Code = 500, Error = "Data is not valid!" });
            ResponseMessage response = new ResponseMessage();

            if (service.Save(ratingDto))
            {
                response.Code = 200;
                response.Body = "Rating is save.";
            }
            else
            {
                response.Code = 500;
                response.Body = "Rating is not save.";
            }

            return Json(response);
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            ResponseMessage response = new ResponseMessage();

            if (service.Delete(id))
            {
                response.Code = 200;
                response.Body = "Rating is deleted.";
            }
            else
            {
                response.Code = 500;
                response.Body = "Rating is not deleted.";
            }

            return Json(response);
        }
    }
}
