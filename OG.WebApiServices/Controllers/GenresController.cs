﻿using OG.ApplicationServices.DTOs;
using OG.ApplicationServices.Implementations;
using OG.WebApiServices.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OG.WebApiServices.Controllers
{
    [Authorize]
    public class GenresController : ApiController
    {
        private readonly GenreManagementService service = null;

        public GenresController()
        {
            service = new GenreManagementService();
        }

        [HttpGet]
        public IHttpActionResult GetAll()
        {
            return Json(service.Get());
        }

        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            return Json(service.GetById(id));
        }

        [HttpPost]
        public IHttpActionResult Save(GenreDto genreDto)
        {
            if (!genreDto.Validate())
                return Json(new ResponseMessage { Code = 500, Error = "Data is not valid!" });
            ResponseMessage response = new ResponseMessage();

            if (service.Save(genreDto))
            {
                response.Code = 200;
                response.Body = "Genre is save.";
            }
            else
            {
                response.Code = 500;
                response.Body = "Genre is not save.";
            }

            return Json(response);
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            ResponseMessage response = new ResponseMessage();

            if (service.Delete(id))
            {
                response.Code = 200;
                response.Body = "Genre is deleted.";
            }
            else
            {
                response.Code = 500;
                response.Body = "Genre is not deleted.";
            }

            return Json(response);
        }
    }
}
