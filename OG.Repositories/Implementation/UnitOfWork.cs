﻿using OG.Data.Context;
using OG.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OG.Repositories.Implementation
{
    public class UnitOfWork : IDisposable
    {
        private OnlineGamesCatalogContext context = new OnlineGamesCatalogContext();
        private GenericRepository<Game> gameRepository;
        private GenericRepository<Genre> genreReposiotry;
        private GenericRepository<Rating> ratingReposiotry;

        public GenericRepository<Game> GameRepository
        {
            get
            {
                if (this.gameRepository == null)
                {
                    this.gameRepository = new GenericRepository<Game>(context);
                }
                return gameRepository;
            }
        }

        public GenericRepository<Genre> GenreReposiotry
        {
            get
            {
                if (this.genreReposiotry == null)
                {
                    this.genreReposiotry = new GenericRepository<Genre>(context);
                }
                return genreReposiotry;
            }
        }

        public GenericRepository<Rating> RatingReposiotry
        {
            get
            {
                if (this.ratingReposiotry == null)
                {
                    this.ratingReposiotry = new GenericRepository<Rating>(context);
                }
                return ratingReposiotry;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
