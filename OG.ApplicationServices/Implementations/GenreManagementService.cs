﻿using OG.ApplicationServices.DTOs;
using OG.Data.Entities;
using OG.Repositories.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OG.ApplicationServices.Implementations
{
    public class GenreManagementService
    {
        public List<GenreDto> Get()
        {
            List<GenreDto> genresDto = new List<GenreDto>();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                foreach (var item in unitOfWork.GenreReposiotry.Get())
                {
                    genresDto.Add(new GenreDto
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Description = item.Description
                    });
                }
            }

            return genresDto;
        }

        public GenreDto GetById(int id)
        {
            GenreDto genreDto = new GenreDto();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Genre genre = unitOfWork.GenreReposiotry.GetByID(id);

                genreDto=new GenreDto
                {
                    Id = genre.Id,
                    Name = genre.Name,
                    Description = genre.Description
                };

            }

            return genreDto;
        }

        public bool Save(GenreDto genreDto)
        {
            Genre genre = new Genre
            {
                Id = genreDto.Id,
                Name = genreDto.Name,
                Description = genreDto.Description
            };

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    if (genreDto.Id == 0)
                        unitOfWork.GenreReposiotry.Insert(genre);
                    else
                        unitOfWork.GenreReposiotry.Update(genre);
                    unitOfWork.Save();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            if (id == 0) return false;

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Genre genre = unitOfWork.GenreReposiotry.GetByID(id);
                    unitOfWork.GenreReposiotry.Delete(genre);
                    unitOfWork.Save();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
