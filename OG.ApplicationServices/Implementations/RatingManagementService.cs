﻿using OG.ApplicationServices.DTOs;
using OG.Data.Entities;
using OG.Repositories.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OG.ApplicationServices.Implementations
{
    public class RatingManagementService
    {
        public List<RatingDto> Get()
        {
            List<RatingDto> ratingsDto = new List<RatingDto>();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                foreach (var item in unitOfWork.RatingReposiotry.Get())
                {
                    ratingsDto.Add(new RatingDto
                    {
                        Id = item.Id,
                        RatingValue = item.RatingValue,
                        Description=item.Description
                    });
                }
            }

            return ratingsDto;
        }

        public RatingDto GetById(int id)
        {
            RatingDto ratingDto = new RatingDto();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Rating rating = unitOfWork.RatingReposiotry.GetByID(id);

                ratingDto = new RatingDto
                {
                    Id = rating.Id,
                    RatingValue = rating.RatingValue,
                    Description = rating.Description
                };

            }
            return ratingDto;
        }

        public bool Save(RatingDto ratingDto)
        {
            Rating rating = new Rating
            {
                Id = ratingDto.Id,
                RatingValue = ratingDto.RatingValue,
                Description = ratingDto.Description
            };

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    if (ratingDto.Id == 0)
                        unitOfWork.RatingReposiotry.Insert(rating);
                    else
                        unitOfWork.RatingReposiotry.Update(rating);
                    unitOfWork.Save();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            if (id == 0) return false;

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Rating rating = unitOfWork.RatingReposiotry.GetByID(id);
                    unitOfWork.RatingReposiotry.Delete(rating);
                    unitOfWork.Save();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
