﻿using OG.ApplicationServices.DTOs;
using OG.Data.Entities;
using OG.Repositories.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OG.ApplicationServices.Implementations
{
    public class GameManagementService
    {
        public List<GameDto> Get()
        {
            List<GameDto> gamesDto = new List<GameDto>();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                foreach (var item in unitOfWork.GameRepository.Get())
                {
                    gamesDto.Add(new GameDto
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Genre = new GenreDto { Id = item.GenreId, Name = item.Genre.Name, Description = item.Genre.Description },
                        Rating = new RatingDto { Id = item.RatingId, Description = item.Rating.Description, RatingValue = item.Rating.RatingValue },
                        ReleaseYear = item.ReleaseYear
                    });
                }
            }

            return gamesDto;
        }

        public GameDto GetById(int id)
        {
            GameDto gameDto = new GameDto();

            using (UnitOfWork unitOfWork = new UnitOfWork())
            {
                Game game = unitOfWork.GameRepository.GetByID(id);

                gameDto = new GameDto
                {
                    Id = game.Id,
                    Name = game.Name,
                    Genre = new GenreDto { Id = game.GenreId, Name = game.Genre.Name, Description = game.Genre.Description },
                    Rating = new RatingDto { Id = game.RatingId, Description = game.Rating.Description, RatingValue = game.Rating.RatingValue },
                    ReleaseYear = game.ReleaseYear
                };

            }
            return gameDto;
        }

        public bool Save(GameDto gameDto)
        {
            Game game = new Game
            {
                Id = gameDto.Id,
                Name = gameDto.Name,
                GenreId = gameDto.Genre.Id,
                RatingId = gameDto.Rating.Id,
                ReleaseYear = gameDto.ReleaseYear
            };

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    if (gameDto.Id == 0)
                        unitOfWork.GameRepository.Insert(game);
                    else
                        unitOfWork.GameRepository.Update(game);
                    unitOfWork.Save();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            if (id == 0) return false;

            try
            {
                using (UnitOfWork unitOfWork = new UnitOfWork())
                {
                    Game game = unitOfWork.GameRepository.GetByID(id);
                    unitOfWork.GameRepository.Delete(game);
                    unitOfWork.Save();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
