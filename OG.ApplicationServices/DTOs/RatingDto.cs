﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OG.ApplicationServices.DTOs
{
    public class RatingDto : BaseDto, IValidate
    {
        public string RatingValue { get; set; }
        public string Description { get; set; }

        public bool Validate()
        {
            return !String.IsNullOrEmpty(RatingValue) && !String.IsNullOrEmpty(Description);
        }
    }
}
