﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OG.ApplicationServices.DTOs
{
    public class GameDto : BaseDto, IValidate
    {
        public string Name { get; set; }
        public string ReleaseYear { get; set; }
        public GenreDto Genre { get; set; }
        public RatingDto Rating { get; set; }

        public bool Validate()
        {
            return !String.IsNullOrEmpty(Name) && !String.IsNullOrEmpty(ReleaseYear);
        }
    }
}
