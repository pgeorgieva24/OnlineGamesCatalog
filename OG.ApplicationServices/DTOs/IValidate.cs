﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OG.ApplicationServices.DTOs
{
    public interface IValidate
    {
        bool Validate();
    }
}
